import com.muscula.core.model.LogParam;
import com.muscula.core.model.MusculaThrowableProxy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        // sending info log with custom object
        logger.info("Info log", new Person("John", 20));

        // sending trace log with multiple custom named parameters
        logger.trace("Trace log", new LogParam("person", new Person("John", 20)), new LogParam("person2", new Person("Julia", 18)));

        // seding error log with exception and custom parameters using MusculaThrowableProxy class
        logger.error("Error log", new MusculaThrowableProxy(new IllegalArgumentException("Age should be bigger or equal to 21"), new Person("Sarah", 18)));
    }

    /**
     * This is sample class represents data which can be passed to any log level.
     */
    public static class Person {
        private String name;
        private int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }
    }
}
